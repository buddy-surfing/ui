import { Component, OnInit, Input } from '@angular/core';
import { RecommendationsService } from '../../services/recommendations.service';
import { ActivatedRoute } from '@angular/router';
import { Recommendation } from '../../model/recommendations';

@Component({
  selector: 'app-recommendations-list',
  templateUrl: './recommendations-list.component.html',
  styleUrls: ['./recommendations-list.component.css']
})
export class RecommendationsListComponent implements OnInit {

  @Input('city')
  public city: string;

  recommendations: Recommendation;

  constructor(private recommendService: RecommendationsService) {}

  ngOnInit() {
    this.recommendService.findAllRecommendations(this.city)
      .subscribe(recom => this.recommendations = recom);
  }
}
