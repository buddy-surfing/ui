import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { City } from '../../model/city';
import { CityService } from '../../services/city.service';

@Component({
  selector: 'app-city-selection',
  templateUrl: './city-selection.component.html',
  styleUrls: ['./city-selection.component.css']
})
export class CitySelectionComponent implements OnInit {
  city: string;
  cityObj: City;

  backgroundImage: string;
  constructor(private activeRoute: ActivatedRoute, cityService: CityService) {
    this.activeRoute.params.subscribe(
      params => {
        this.city = params.city;
        if (this.city === 'Rosario') {
          this.backgroundImage = environment.apiUrl + '/cities/large/rosario.jpg';
        } else {
          this.backgroundImage = environment.apiUrl + '/cities/large/amsterdam.jpg';
        }
      }
    );
  }

  ngOnInit() {
  }
}
