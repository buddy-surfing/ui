import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioModal1Component } from './portfolio-modal1.component';

describe('PortfolioModal1Component', () => {
  let component: PortfolioModal1Component;
  let fixture: ComponentFixture<PortfolioModal1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioModal1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioModal1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
