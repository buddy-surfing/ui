import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-video-block',
  templateUrl: './video-block.component.html',
  styleUrls: ['./video-block.component.css']
})
export class VideoBlockComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.video-bg').wallpaper({
      source: {
        mp4: 'assets/mp4/traffic.mp4',
        poster: 'assets/img/demo-bgs/video-bg-fallback.jpg'
      }
    });
  }
}
