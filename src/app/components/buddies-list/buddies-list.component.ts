import { Component, Input, OnInit } from '@angular/core';
import { Buddy } from 'src/app/model/buddy';
import { BuddyService } from '../../services/buddy.service';

@Component({
  selector: 'app-buddies-list',
  templateUrl: './buddies-list.component.html',
  styleUrls: ['./buddies-list.component.css']
})
export class BuddiesListComponent implements OnInit {
  @Input('city')
  public city: string;

  buddies: Buddy[];
  constructor(private buddyService: BuddyService) { }

  ngOnInit() {
    this.buddyService.findAllBuddies(this.city)
      .subscribe(bud => {
        this.buddies = bud;
      });
  }
}
