import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioModal2Component } from './portfolio-modal2.component';

describe('PortfolioModal2Component', () => {
  let component: PortfolioModal2Component;
  let fixture: ComponentFixture<PortfolioModal2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioModal2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioModal2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
