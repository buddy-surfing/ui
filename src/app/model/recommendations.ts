export class Recommendation {
  place_name: string;
  tab_category: string[];
  address: string;
  description: string;
  like: number;
}
