export class City {
    city_name: string;
    country_name: string;
    continent: string;
    image_url: string;
    image_large_url: string;
}
