import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recommendation } from '../model/recommendations';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecommendationsService {

  constructor(private httpClient: HttpClient) { }

  public findAllRecommendations(cityName: string) {
    const url = environment.apiUrl + 'api/Recommendations/' + cityName;
    return this.httpClient.get<Recommendation>(url);
  }
}
