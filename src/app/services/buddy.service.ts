import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Buddy } from '../model/buddy';

@Injectable({
  providedIn: 'root'
})
export class BuddyService {

  constructor(private http: HttpClient) { }

  public findAllBuddies(city: string) {
    const url = environment.apiUrl + 'api/Buddies/' + city;
    return this.http.get<Buddy[]>(url);
  }
}
