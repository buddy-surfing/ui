import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { City } from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) { }

  public findAllCities() {
    const url = environment.apiUrl + 'api/Cities';
    return this.http.get<City[]>(url);
  }

  public findCity(city: string) {
    const url = environment.apiUrl + 'api/Cities/' + city;
    return this.http.get<City>(url);
  }
}
