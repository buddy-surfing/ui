import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RecommendationsService } from './services/recommendations.service';
import { BuddyService } from './services/buddy.service';
import { CityService } from './services/city.service';

import { WrapperComponent } from './components/wrapper/wrapper.component';
import { PortfolioModal1Component } from './components/portfolio-modal1/portfolio-modal1.component';
import { PortfolioModal2Component } from './components/portfolio-modal2/portfolio-modal2.component';
import { SearchComponent } from './components/search/search.component';
import { CityComponent } from './components/city/city.component';
import { FeaturesComponent } from './components/features/features.component';

import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecommendationsListComponent } from './components/recommendations-list/recommendations-list.component';
import { ImagePipe } from './pipes/image.pipe';
import { CitySelectionComponent } from './components/city-selection/city-selection.component';
import { BuddiesListComponent } from './components/buddies-list/buddies-list.component';
import { LayoutComponent } from './layout/layout.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { VideoBlockComponent } from './components/video-block/video-block.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';

const appRoutes: Routes = [
  {
    path: '',
    component: WrapperComponent
  },
  {
    path: 'City/:city',
    component: CitySelectionComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    WrapperComponent,
    PortfolioModal1Component,
    PortfolioModal2Component,
    CityComponent,
    FeaturesComponent,
    NotFoundComponent,
    RecommendationsListComponent,
    ImagePipe,
    CitySelectionComponent,
    BuddiesListComponent,
    LayoutComponent,
    NavBarComponent,
    VideoBlockComponent,
    AboutComponent,
    ContactComponent,
    FooterComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    BuddyService,
    RecommendationsService,
    CityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
